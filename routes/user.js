const express = require("express");
const router = express.Router();

const userCtrl = require("../controllers/userController");

router.get("/", userCtrl.fetchAll);
router.get("/:id", userCtrl.fetchOneById);

module.exports = router;
