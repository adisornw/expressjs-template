const userService = require("../services/userService");
module.exports = {
  fetchAll: async (req, res) => {
    const users = userService.fetchUsers();
    return res.json(users);
  },
  fetchOneById: async (req, res, next) => {
    const userId = req.params.id;
    const user = userService.fetchOneById(userId);
    return res.json(user);
  },
};
