const users = [
  {
    id: 1,
    name: "Game",
  },
  { id: 2, name: "Ice" },
];
module.exports = {
  fetchUsers() {
    return users;
  },
  fetchOneById(id) {
    return users.find((user) => user.id == id);
  },
};
