/*
cors สำหรับจัดการเกี่ยวกับ Connection ระหว่าง Client และ Server
dotenv สำหรับจัดการเกี่ยวกับ Environment Variable
express สำหรับจัดการเกี่ยวกับ Request ต่างๆ
express-jwt สำหรับจัดการเกี่ยวกับการ Validates JsonWebTokens
moment สำหรับจัดการเกี่ยวกับเวลา
morgan สำหรับจัดการเกี่ยวกับ Log
*/

const env = process.env.NODE_ENV || "development";
const PORT = process.env.PORT || 5001;
const compression = require("compression");
const express = require("express");
const app = express();
const logger = require("morgan");
const bodyParser = require("body-parser");
const helmet = require("helmet");
const cors = require("cors"); // for cros server

app.use(helmet());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

if (process.env.NODE_ENV === "production") {
  //connect db prod
} else if (process.env.NODE_ENV === "test") {
  //connect db test
} else {
  //connect db dev
}

app.use(bodyParser.json());
app.use(compression());
app.use(logger("dev"));

// check authen
const authenticaton = require("./middlewares/authentication");
app.use(authenticaton);

// routes
const user = require("./routes/user");
app.use(`/users`, user);

app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`));
